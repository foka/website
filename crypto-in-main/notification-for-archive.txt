To: crypt@bxa.doc.gov
Subject: Initial one-time notification for Debian GNU/Linux

                                                      Department of Commerce
                                             Bureau of Export Administration
                       Office of Strategic Trade and Foreign Policy Controls
                                     14th Street and Pennsylvania Ave., N.W.
                                                                   Room 2705
                                                        Washington, DC 20230

Re:  Unrestricted Encryption Source Code Notification
Commodity: Debian Source Code

Dear Sir/Madam,
     Pursuant to paragraph (e)(1) of Part 740.13 of the U.S. Export
Administration Regulations ("EAR", 15 CFR Part 730 et seq.), we are
providing this written notification of the Internet location of the
unrestricted, publicly available Source Code of packages in the Debian
operating system.  Debian is a free operating system developed by a group
of individuals, coordinated by the non-profit Software in the Public
Interest.  This archive is updated from time to time, but its location
is constant.  At present, the software listed in this notification does
not include cryptographic functionality, however we expect cryptographic
functionality to be added to much of it in future.  Therefore this
notification serves as a one-time notification for subsequent updates
that may occur in the future to software covered by this notification.
Such updates may add or enhance cryptographic functionality of the
Debian operating system.  Additional notifications will be submitted as
new components are added to the Debian operating system.  The Internet
location for the Debian Source Code is: http://ftp.debian.org/debian/

This site is mirrored to a number of other sites located both within
and outside the United States.

Descriptions of the contents of the Debian archive are attached.

Further information about the Debian project is available at
http://www.debian.org/

If you have any questions, please contact Ben Collins, via email at
xxx@xxx, or telephone on (XXX) XXX-XXXX.

     Sincerely,
        Ben Collins (Project Leader)
        and Anthony Towns (Release Manager)
        for the Debian Project
